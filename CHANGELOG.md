# Changelog

## Unreleased

### Fixed

- spip/spip#6022 Utiliser des div pour les réponses de formulaire, pas de marge basse sur les derniers éléments des
  réponses.

## 4.3.0 - 2026-11-26

### Security

- spip-team/securite#4853 Appliquer un filtre `attribut_url()` aux endroits pertinents

### Added

- spip/spip#5566 : le dossier `javascript/_inits/` reçoit les fichiers d'initialisation JS pour les formulaires et blocs ajaxés
- Installable en tant que package Composer
- #4865 Utilisation de variables CSS

### Changed

- #4861 Balisage respectant mieux HTML5

### Fixed

- !4885 Correction de coquille dans les flux RSS (sur `<thr:in-reply-to>`)
- #4879 Suppression des parenthèses autour de l'url du site dans le mail envoyé à l'auteur

### Removed

- #4875 Squelettes `backend-breves.html` et `breve.html` (intégrées dans le plugin Brèves)
- spip/filtres_images#4723 Police `Dustimo` (utiliser le plugin `images_typo`)
