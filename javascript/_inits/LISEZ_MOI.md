Le dossier `javascript/_inits/` reçoit des fichiers d'initialisation javascript qui seront chargés automatiquement via `#INSERT_HEAD` 
Préfixer un numéro permet de définir un index pour l'ordre d'appel 
Un fichier vide indique qu'il faut charger le contenu d'un fichier homonyme disponible depuis l'espace privé
Si vous avez copié `squelettes-dist/` vers `squelettes/` comme recommandé, et que vous désirez désactiver un appel, ne supprimez pas le fichier en question, surchargez-le avec un contenu qui ne fait rien, ex. : `// rien`

